#!/bin/sh

# add epel
yum -y install epel-release

# add misc packages
yum -y install telnet mtr tcpdump nmap nc

# set custom prompt for vagrant uid
echo '
tput sgr0 # Reset colors
bold="\["$(tput bold)"\]"
reset="\["$(tput sgr0)"\]"

# Solarized colors
# (https://github.com/altercation/solarized/tree/master/iterm2-colors-solarized#the-values)
black="\["$(tput setaf 0)"\]"
blue="\["$(tput setaf 33)"\]"
cyan="\["$(tput setaf 37)"\]"
green="\["$(tput setaf 190)"\]"
orange="\["$(tput setaf 172)"\]"
purple="\["$(tput setaf 141)"\]"
red="\["$(tput setaf 124)"\]"
violet="\["$(tput setaf 61)"\]"
magenta="\["$(tput setaf 9)"\]"
white="\["$(tput setaf 8)"\]"
yellow="\["$(tput setaf 136)"\]"

export PS1="\[\033]0;\u@\h\007\]$white[$violet\u@\h$white:$yellow\W$white]$magenta\$ $reset"'  >>/home/vagrant/.bash_profile